#!/bin/bash
echo Test Script

echo "Installing software"
sudo yum -y install samba samba-client samba-common

echo "Services.."
sudo systemctl enable smb.service
sudo systemctl enable nmb.service
sudo systemctl restart smb.service
sudo systemctl restart nmb.service

echo "Firewall.."
sudo firewall-cmd --permanent --zone=public --add-service=samba
sudo firewall-cmd --reload

echo "Useradd"
sudo useradd -s /usr/sbin/nologin franka
sudo useradd -s /usr/sbin/nologin femkevdv
sudo useradd -s /usr/sbin/nologin hansb
sudo useradd -s /usr/sbin/nologin kimberlyvh
sudo useradd -s /usr/sbin/nologin taniav
sudo useradd -s /usr/sbin/nologin peterj
sudo useradd maaiked

echo "Set password"
sudo chpasswd << 'END'
franka:franka
femkevdv:femkevdv
hansb:hansb
kimberlyvh:kimberlyvh
taniav:taniav
peterj:peterj
maaiked:maaiked
END

echo "Add groups"
sudo groupadd directie
sudo groupadd financieringen
sudo groupadd staf
sudo groupadd verzekeringen
sudo groupadd publiek
sudo groupadd beheer

echo "Usermod"
sudo usermod -a -G directie,staf,financieringen,verzekeringen,publiek franka
sudo usermod -a -G staf,publiek femkevdv
sudo usermod -a -G verzekeringen,publiek hansb
sudo usermod -a -G verzekeringen,publiek kimberlyvh
sudo usermod -a -G verzekeringen,publiek taniav
sudo usermod -a -G verzekeringen,publiek peterj
sudo usermod -a -G directie,financieringen,staf,verzekeringen,publiek,beheer maaiked
sudo usermod -a -G directie,financieringen,staf,verzekeringen,publiek,beheer mattias

echo "Add user to samba"
(echo franka; echo franka) | sudo smbpasswd -s -a franka
(echo femkevdv; echo femkevdv) | sudo smbpasswd -s -a femkevdv
(echo hansb; echo hansb) | sudo smbpasswd -s -a hansb
(echo kimberlyvh; echo kimberlyvh) | sudo smbpasswd -s -a kimberlyvh
(echo taniav; echo taniav) | sudo smbpasswd -s -a taniav
(echo peterj; echo peterj) | sudo smbpasswd -s -a peterj
(echo maaiked; echo maaiked) | sudo smbpasswd -s -a maaiked
(echo mattias; echo mattias) | sudo smbpasswd -s -a mattias

echo "Enable samba users"
sudo smbpasswd -e franka
sudo smbpasswd -e femkevdv
sudo smbpasswd -e hansb
sudo smbpasswd -e kimberlyvh
sudo smbpasswd -e taniav
sudo smbpasswd -e peterj
sudo smbpasswd -e maaiked
sudo smbpasswd -e mattias

echo "Create shares"
sudo mkdir /srv/shares
sudo mkdir /srv/shares/directie
sudo mkdir /srv/shares/financieringen
sudo mkdir /srv/shares/staf
sudo mkdir /srv/shares/verzekeringen
sudo mkdir /srv/shares/publiek
sudo mkdir /srv/shares/beheer

echo "Take ownership"
sudo chown -R root:directie /srv/shares/directie
sudo chown -R root:financieringen /srv/shares/financieringen
sudo chown -R root:staf /srv/shares/staf
sudo chown -R root:verzekeringen /srv/shares/verzekeringen
sudo chown -R root:publiek /srv/shares/publiek
sudo chown -R root:beheer /srv/shares/beheer

echo "Set permissions"
sudo chmod 775 /srv/shares/directie/
sudo chmod 775 /srv/shares/financieringen/
sudo chmod 775 /srv/shares/staf/
sudo chmod 775 /srv/shares/verzekeringen/
sudo chmod 775 /srv/shares/publiek/
sudo chmod 770 /srv/shares/beheer/

echo "Set hostname"
hostnamectl set-hostname filesrv.linuxlab.lan

echo "Set smb.conf"
sudo cp /vagrant/smb.conf /etc/samba/smb.conf

echo "Restart smb.service"
sudo systemctl restart smb.service















