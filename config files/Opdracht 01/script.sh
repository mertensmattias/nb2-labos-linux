#!/bin/bash
echo Test Script

echo "Updating en installing software for http, mysql, php, ssl en wordpress"
sudo yum update -y 
sudo yum install -y httpd php php-mysql mariadb-server mariadb php-gd mod_ssl

echo "Start and enable httpd.service.."
sudo systemctl start httpd.service
sudo systemctl enable httpd.service

echo "Start and enable mariadb.service.."
sudo systemctl start mariadb.service
sudo systemctl enable mariadb.service

echo "Restarting httpd.service"
sudo systemctl restart httpd.service

echo "Configuring firewall.."
sudo firewall-cmd --permanent --add-service=ssh
sudo firewall-cmd --permanent --add-service=http
sudo firewall-cmd --permanent --add-service=https
sudo firewall-cmd --permanent --add-service=mysql

echo "Restarting firewall.."
systemctl restart firewalld.service

# "Aangepast script van: http://stackoverflow.com/questions/19403370/installing-mysql-on-centos-6-vm-vagrant-with-bash-script

echo "UPDATE mysql.user SET Password=PASSWORD('vagrant') WHERE User='root';" > /tmp/init.sql
echo "DELETE FROM mysql.user WHERE User='';" >> /tmp/init.sql
echo "DROP DATABASE test;"  >> /tmp/init.sql
echo "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';" >> /tmp/init.sql
echo "CREATE DATABASE wordpress;" >> /tmp/init.sql
echo "CREATE USER 'wordpressuser'@'127.0.0.1' IDENTIFIED BY 'vagrant';"  >> /tmp/init.sql
echo "CREATE USER 'wordpressuser'@localhost IDENTIFIED BY 'vagrant';" >> /tmp/init.sql
echo "GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpressuser'@'127.0.0.1';" >> /tmp/init.sql
echo "GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpressuser'@'localhost';" >> /tmp/init.sql
echo "flush privileges;" >> /tmp/init.sql

echo Uitvoeren van init.sql onder mysql -u root
sudo cat /tmp/init.sql | mysql -u root

echo "Download, Unpack and copy wordpress to /var/www/html/"
cd ~
wget http://wordpress.org/latest.tar.gz
tar xzvf latest.tar.gz
sudo rsync -avP ~/wordpress/ /var/www/html/

echo "Create content directory"
mkdir /var/www/html/wp-content/uploads
cp /vagrant/wp-config.php /var/www/html/wp-config.php

echo "Giving ownership to apache:apache"
sudo chown -R apache:apache /var/www/html/*

echo "Self-signed cert en HTTPS voor Wordpress installatie"
echo "Copy Cert Files.."
sudo cp /vagrant/ca.crt /etc/pki/tls/certs/
sudo cp /vagrant/ca.key /etc/pki/tls/private/ca.key
sudo cp /vagrant/ca.csr /etc/pki/tls/private/ca.csr

echo "Copy ssl.conf.."
sudo cp /vagrant/ssl.conf /etc/httpd/conf.d/ssl.conf

echo "Restarting httpd.service.."
sudo systemctl restart httpd.service










