## Laboverslag: Werkomgeving

Naam cursist: Mattias Mertens

Bitbucket repo: https://bitbucket.org/mertensmattias/nb2-labos-linux/

### Configuratie van de CentOS installatie.
	
	Netwerkinstellingen voor Virtualbox
	1:	NAT 10.0.2.0/24 met DHCP
	2:	Host-Only 192.168.15.0/24 met DHCP
	
	Instellingen voor CentOS
	1:	Minimal Installation
	2:	Taalkeuze (English US) (Installer)
	3:	Toetsenbord instellingen (Installer)
	4:	Voor automatische partitioning gekozen (Installer)
	
	Users
	1:	Root paswoord ingesteld op 'vagrant'
	2:	User 'vagrant' en 'mattias' aangemaakt met paswoord 'vagrant' 
	3:	Beide zijn member van de group 'Wheel'
	
	
	Netwerk configuratie van CentOS
	
	NAT Adapter config(enp0s3)
	
	TYPE="Ethernet"
	BOOTPROTO="DHCP"
	NM_CONTROLLED="no"
	DEVICE="enp0s3"
	ONBOOT="yes"

	Host-Only adapter config (enp0s8)
	
	TYPE="Ethernet"
	BOOTPROTO="DHCP"
	NM_CONTROLLED="no"
	DEVICE="enp0s8"
	ONBOOT="yes"

	
	SSH configuratie met key authenticatie
	
	Algemene configuratie voor /etc/ssh/sshd_config
	
	port 22
	PermitRootlogin no
	PubkeyAuthentication yes
	
	Voor de user mattias.
	1:	ssh-keygen, dit maakt je privat en public key aan . 
	2:	mv ~/.ssh/id_rsa.pub authorized_keys
	3:	chmod 700 ~/.ssh/
	4:	chmod 600 ~/.ssh/authorized_keys
	
	De privé key gekopieerd naar mijn lokaal systeem en gebruikt in combinatie met een ssh client. 
	
	Voor de user vagrant, gebrukte ik de standaard unsecure public key.
	1:	 curl https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub >> /home/vagrant/.ssh/authorized_keys
	2:	chmod 700 ~/.ssh/
	3:	chmod 600 ~/.ssh/authorized_keys
	4:	chown -R vagrant:vagrant /home/vagrant/
	5:	systemctl restart sshd.service
	
	
	
	Installatie van de gevraagde software: bash-completion, bind-utils, nano, vim-enhanced, wget, man, git, curl, ntp

	Git Bash gelinkt via ssh met mijn bitbucket account. 
   
	Van deze installatie is er een Vagrant base box gemaakt. Hiervoor heb ik de een goede tutorial gevonden (Zie referenties)

 
#### Wat ging goed?

	1:	Installatie van CentOS zelf. 
	2:	Configuratie van ssh met key authenticatie.
	3:	Linken van bitbucket met mijn windows account via ssh (bit bash).
	4:	Aanmaken van de vagrant base box.


#### Wat ging niet goed?

	1:	Virtualbox netwerk configuratie. Dit heeft me de nodige uren gekost om het opgelost te krijgen. Ik had last 
		van een bug uit 2007. Uiteindelijk Check-point VPN moeten deinstalleren. Na een herinstallatie van Virtualbox waren de problemen opgelost.
	2:	Het installeren van de VirtualBoxAddons, deze installeren zich niet correct. Hiervoor heb ik wel
		een oplossing gevonden. 
	3:	Het nakijken en configureren (boot on startup) van de netwerk instellingen. (dit bleek uiteindelijk te liggen aan de bug van hierboven) 
	4:	Command-line van CentOS, de switch tussen Debian based besturings systemen en één met een fedora core is stevig aanpassen.


#### Wat heb je geleerd?

	Het meeste heb geleerd over Git en vagrant. 

#### Waar heb je nog problemen mee?

	Al de problemen zijn opgelost. 

### Referenties

	ssh login SSH http://www.server-world.info/en/note?os=CentOS_7&p=ssh&f=4
	ssh login Git https://confluence.atlassian.com/display/BITBUCKET/Set+up+SSH+for+Git#SetupSSHforGit-Step4.Createaconfigfile
	Aanmaken van Vagrant box: http://www.hostedcgi.com/how-to-create-a-centos-7-0-vagrant-base-box/
	Installeren van GuestAddons: http://www.if-not-true-then-false.com/2010/install-virtualbox-guest-additions-on-fedora-centos-red-hat-rhel/
	VirtualboxGuestAddons bug: http://stackoverflow.com/questions/28494349/vagrant-failed-to-mount-folders-in-linux-guest-vboxsf-file-system-is-not-av

