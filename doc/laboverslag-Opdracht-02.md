## Laboverslag: Opdracht 2: Samba file server

Naam cursist: Mattias Mertens

Bitbucket repo: https://bitbucket.org/mertensmattias/nb2-labos-linux/

### Intro
	
	Deze opdracht maakt weer gebruik van de vagrant base box uit opdracht 00.
	Je kan de gebruikte config, script en vagrant files terugvinden in mijn repository.
	
	
	Base box 
	nb2-labos-linux / vagrant box / CentOS1.1
	
	Config files
	nb2-labos-linux / config files / Opdracht 02 / 
	
###	Kort overlopen van het provisioning script

	1:	Installatie van de nodige software
	
	2:	Starten en activeren van de nmb & smb service
	
	3:	Firewall rules toevoegen voor Samba
	
	4:	Useradd gebruikt om de users toe te voegen. 
		Hierbij was de vraag om enkel mezelf en maaiked shell access te geven. Ikzelf (mattias) bestaat al.
		sudo useradd -s /usr/sbin/nologin franka
		sudo useradd -s /usr/sbin/nologin femkevdv
		sudo useradd -s /usr/sbin/nologin hansb
		sudo useradd -s /usr/sbin/nologin kimberlyvh
		sudo useradd -s /usr/sbin/nologin taniav
		sudo useradd -s /usr/sbin/nologin peterj
		sudo useradd maaiked
		
	5:	Paswoord instellingen zijn gedaan voor de users(Password = Username).
	
		sudo chpasswd << 'END'
		franka:franka
		femkevdv:femkevdv
		hansb:hansb
		kimberlyvh:kimberlyvh
		taniav:taniav
		peterj:peterj
		maaiked:maaiked
		END
	
	6:	De nodige groepen toegevoegd.
	
	7: 	De users toegevoegd aan de correcte groupen om later hun toegang tot de shares te kunnen bepalen.
	
		sudo usermod -a -G directie,staf,financieringen,verzekeringen,publiek franka
		sudo usermod -a -G staf,publiek femkevdv
		sudo usermod -a -G verzekeringen,publiek hansb
		sudo usermod -a -G verzekeringen,publiek kimberlyvh
		sudo usermod -a -G verzekeringen,publiek taniav
		sudo usermod -a -G verzekeringen,publiek peterj
		sudo usermod -a -G directie,financieringen,staf,verzekeringen,publiek,beheer maaiked
		sudo usermod -a -G directie,financieringen,staf,verzekeringen,publiek,beheer mattias
		
	8:	Dezelfde users toegevoegd aan Samba
	
		(echo franka; echo franka) | sudo smbpasswd -s -a franka
		(echo femkevdv; echo femkevdv) | sudo smbpasswd -s -a femkevdv
		(echo hansb; echo hansb) | sudo smbpasswd -s -a hansb
		(echo kimberlyvh; echo kimberlyvh) | sudo smbpasswd -s -a kimberlyvh
		(echo taniav; echo taniav) | sudo smbpasswd -s -a taniav
		(echo peterj; echo peterj) | sudo smbpasswd -s -a peterj
		(echo maaiked; echo maaiked) | sudo smbpasswd -s -a maaiked
		(echo mattias; echo mattias) | sudo smbpasswd -s -a mattias
		
	9:	Activeren van de Samba users
	
		sudo smbpasswd -e franka
		sudo smbpasswd -e femkevdv
		sudo smbpasswd -e hansb
		sudo smbpasswd -e kimberlyvh
		sudo smbpasswd -e taniav
		sudo smbpasswd -e peterj
		sudo smbpasswd -e maaiked
		sudo smbpasswd -e mattias
	
	10:Aaanmaken van de nodige shares op /srv/shares
	
	11:	Correcte ownership gegeven aan deze shares.
	
		sudo chown -R root:directie /srv/shares/directie
		sudo chown -R root:financieringen /srv/shares/financieringen
		sudo chown -R root:staf /srv/shares/staf
		sudo chown -R root:verzekeringen /srv/shares/verzekeringen
		sudo chown -R root:publiek /srv/shares/publiek
		sudo chown -R root:beheer /srv/shares/beheer
	
	12: Correcte folder permissions
	
		sudo chmod 775 /srv/shares/directie/
		sudo chmod 775 /srv/shares/financieringen/
		sudo chmod 775 /srv/shares/staf/
		sudo chmod 775 /srv/shares/verzekeringen/
		sudo chmod 775 /srv/shares/publiek/
		sudo chmod 770 /srv/shares/beheer/
		
	13:	Overzetten van de correcte smb.conf file.
	
	14: Herstarten van SMB
	
	
	
#### Wat ging goed?

	Het opzetten van deze omgeving met een bash script wat zeker een stuk meer tijd kost dan 
	ansible te gebruiken. 
	
#### Wat ging niet goed?

	Het werken met ansible. Hiervoor heb je een werkende, liefst niet virtuele, linux omgeving voor nodig.
	De pogingen om er toch mee te werken hebben me dan ook redelijk gefrustreerd gekregen. 
	
	Ook had ik problemen om het bats script aan de praat te krijgen. 

#### Waar heb je nog problemen mee?

	Ansible?

#### Referenties
	1:	http://daddy-linux.blogspot.be/2012/02/user-and-group-management-command.html
	2:	http://linux.die.net/man/8/useradd
	3:	http://www.ehow.com/how_8424335_create-user-login-ubuntu.html
	4:	https://www.samba.org/samba/docs/man/Samba-HOWTO-Collection/
