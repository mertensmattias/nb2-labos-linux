## Laboverslag: 01 - Webserver

Naam cursist: Mattias Mertens

Bitbucket repo: https://bitbucket.org/mertensmattias/nb2-labos-linux/

### Procedures

1: 	Deze opdracht is gebaseerd op de base box die ik aangemaakt heb in de eerste opdracht. 
	Je kan de gebruikte config, script en vagrant files terugvinden in mijn repository.
	
	Base box 
	nb2-labos-linux / vagrant box / CentOS1.101
	
	Config files
	nb2-labos-linux / config files / Opdracht 01 / 
	
### Overlopen Vagrantfile 
	
	1:	De gebruikte box.
	
		config.vm.box = "CentOS1.1"
	
	2:	Private network adapter aangemaakt, hier heb ik een IP moeten opgeven, dit ook vanwege een bug in Vagrant
		Als ik hier de optie "DHCP" gebruik gaat hij telkens een nieuwe netwerk adapter configuren in windows.
	
		config.vm.network "private_network", ip:"192.168.15.101", auto_config: false
	
		Het gebruikte script voor provisioning.
	
		config.vm.provision "shell", path: "script.sh"
		
### Provisioning met behulp van een bash script. Hieronder kan je mijn script terug vinden, de stappen zouden duidelijk moeten zijn. 
	
	#!/bin/bash
	
	echo "Updating en installing software for http, mysql, php, ssl en wordpress"
	sudo yum update -y 
	sudo yum install -y httpd php php-mysql mariadb-server mariadb php-gd mod_ssl

	echo "Start and enable httpd.service.."
	sudo systemctl start httpd.service
	sudo systemctl enable httpd.service

	echo "Start and enable mariadb.service.."
	sudo systemctl start mariadb.service
	sudo systemctl enable mariadb.service

	echo "Restarting httpd.service"
	sudo systemctl restart httpd.service

	echo "Configuring firewall.."
	sudo firewall-cmd --permanent --add-service=ssh
	sudo firewall-cmd --permanent --add-service=http
	sudo firewall-cmd --permanent --add-service=https
	sudo firewall-cmd --permanent --add-service=mysql

	echo "Restarting firewall.."
	systemctl restart firewalld.service

	# "Aangepast script van: http://stackoverflow.com/questions/19403370/installing-mysql-on-centos-6-vm-vagrant-with-bash-script

	echo "UPDATE mysql.user SET Password=PASSWORD('vagrant') WHERE User='root';" > /tmp/init.sql
	echo "DELETE FROM mysql.user WHERE User='';" >> /tmp/init.sql
	echo "DROP DATABASE test;"  >> /tmp/init.sql
	echo "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';" >> /tmp/init.sql
	echo "CREATE DATABASE wordpress;" >> /tmp/init.sql
	echo "CREATE USER 'wordpressuser'@'127.0.0.1' IDENTIFIED BY 'vagrant';"  >> /tmp/init.sql
	echo "CREATE USER 'wordpressuser'@localhost IDENTIFIED BY 'vagrant';" >> /tmp/init.sql
	echo "GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpressuser'@'127.0.0.1';" >> /tmp/init.sql
	echo "GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpressuser'@'localhost';" >> /tmp/init.sql
	echo "flush privileges;" >> /tmp/init.sql

	echo "Uitvoeren van init.sql onder mysql -u root"
	sudo cat /tmp/init.sql | mysql -u root

	echo "Download, Unpack and copy wordpress to /var/www/html/"
	cd ~
	wget http://wordpress.org/latest.tar.gz
	tar xzvf latest.tar.gz
	sudo rsync -avP ~/wordpress/ /var/www/html/

	echo "Create content directory"
	mkdir /var/www/html/wp-content/uploads
	cp /vagrant/wp-config.php /var/www/html/wp-config.php

	echo "Giving ownership to apache:apache"
	sudo chown -R apache:apache /var/www/html/*

	echo "Self-signed cert en HTTPS voor Wordpress installatie"
	echo "Copy Cert Files.."
	sudo cp /vagrant/ca.crt /etc/pki/tls/certs/
	sudo cp /vagrant/ca.key /etc/pki/tls/private/ca.key
	sudo cp /vagrant/ca.csr /etc/pki/tls/private/ca.csr

	echo "Copy ssl.conf.."
	sudo cp /vagrant/ssl.conf /etc/httpd/conf.d/ssl.conf

	echo "Restarting httpd.service.."
	sudo systemctl restart httpd.service
	

	
### Testplan en -rapport

	Mijn werkwijze was eenvoudig. Met de base box als basis ben ik begonnen met een provisioning script
	te schrijven. Ik heb ervoor gekozen om met bash te werken daar dit voor mij de meest werkbare oplossing is. 
	
	Elke stap, is eerst handmatig getest in de basis installatie. Indien succesvol kwam het terecht in mijn script file. Na 
	elke stap heb ik dit script ook laten lopen om te kijken of ook dit werkt zoals gewenst. 
	
	Dit script gaat zorgen voor volgende punten.
	1:	Installatie van de nodige software.
	2:	Het starten en activeren van httpd en mariadb services
	3:	Firewall configuratie
	4:	Aanmaken en toevoegen van een database en user.
	5:	Installatie van de nieuwste versie van Wordpress via wget
	6:	Toevoegen van https. 
	 

#### Wat ging goed?
	
	Het werken met vagrant, tijdens het maken van deze opdracht is de kracht van deze software echt wel duidelijk geworden.
	Het testen en configureren van de installatie is zo eenvoudig op deze manier. Ik ga hierzelf zeker meer mee aan de slag.

#### Wat ging niet goed?

	Met de opdracht waren er weinig problemen, dit is niet de eerste keer. Voor mij is het redelijk duidelijk waar te kijken en hoe
	dit aangepakt moet worden. 
	
	De grootste problemen waren vooral gerelateerd met Virtualbox/Windows combinatie. 
	Bv, om de base box aan te maken moesten de VirtualboxGuestAddons geinstalleerd worden. Deze installatie verloop niet volledig correct en
	vagrant herkende de Addon niet. Hiervoor heb ik dan wel een oplossing via google kunnen vinden (zie referenties).
	
	Virtualbox bleek ook mijn tweede netwerk kaart niet te configueren. De configuratie van de guest was in orde
	maar mijn Host-only adapter bleek nooit actief te komen. Dit is opgelost door een Host-only netwerk te definieeren in vagrantfile.
	Door het plaatsen van de gewenste IP kon ik ook vermijden dat vagrant elke keer een nieuwe netwerk kaart aanmaakt in Windows met telkens
	een andere IP range. Hiermee ben ik wel een tijd bezig geweest voor het resultaat naar wens was.
	
	

### Referenties

	Aanmaken Vagrant box: http://www.hostedcgi.com/how-to-create-a-centos-7-0-vagrant-base-box/
	Intalleren GuestAddons: http://www.if-not-true-then-false.com/2010/install-virtualbox-guest-additions-on-fedora-centos-red-hat-rhel/
	Oplossen problemen GuestAddons: http://stackoverflow.com/questions/28494349/vagrant-failed-to-mount-folders-in-linux-guest-vboxsf-file-system-is-not-av
	Shell scripting en Vagrant: http://www.sitepoint.com/vagrantfile-explained-setting-provisioning-shell/
	Installatie Wordpress: https://www.digitalocean.com/community/tutorials/how-to-install-wordpress-on-centos-7
	Https: http://wiki.centos.org/HowTos/Https